package com.algaworks.erp.controller;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.algaworks.erp.model.Cliente;
import com.algaworks.erp.model.Jogo;
import com.algaworks.erp.repository.Clientes;
import com.algaworks.erp.service.CadastroClienteService;
import com.algaworks.erp.util.FacesMessages;

@Named("gestaoClientesBean")
@ViewScoped
public class GestaoClientesBean implements Serializable {

private static final long serialVersionUID = 1L;
	
	@Inject
	private Clientes clientes;
	
	@Inject
    private FacesMessages messages;
	
	@Inject
	private CadastroClienteService cadastroClienteService;
	
	private List<Cliente> listaClientes;
	
	@Inject
	private Cliente cliente;
	
	public Cliente getCliente() {
		return cliente;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public void prepararNovoCliente() {
		cliente = new Cliente();
	}
	
	public void prepararEdicaoCliente() {
		cliente = new Cliente();
	}
	
	public void salvar() {
		System.out.println(cliente.getNome());
//		cadastroClienteService.salvar(cliente);
//		messages.info("Cliente salvo com sucesso!");
//		RequestContext.getCurrentInstance().update(Arrays.asList(
//			"frm:jogosDataTable", 
//			"frm:messages"
//		));
//		todosJogos();
	}

	public void excluir() {
		cadastroClienteService.excluir(cliente);
		cliente = null;
		todosClientes();
		messages.info("Cliente deletado com sucesso!");
	}
	
	public void todosClientes() {
		listaClientes = clientes.todas();
	}
	
	public List<Cliente> getListaClientes() {
		List<Cliente> listaClientes = clientes.todas();
		return listaClientes;
	}
	
	
	
	public boolean isClienteSeleciona() {
		return cliente != null && cliente.getId() != null;
	}
}
