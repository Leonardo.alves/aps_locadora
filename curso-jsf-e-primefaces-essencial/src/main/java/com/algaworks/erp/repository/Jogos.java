package com.algaworks.erp.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.algaworks.erp.model.Jogo;

public class Jogos implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Jogos() {

	}
	
	public Jogos(EntityManager manager) {
		this.manager = manager;
	}
	
	public List<Jogo> todas() {
		 return manager.createQuery("from Jogo", Jogo.class).getResultList();
	}
	
	public Jogo porId(Long id) {
		return manager.find(Jogo.class, id);
	}
	
	public Jogo guardar(Jogo jogo) {
		return manager.merge(jogo);
	}

	public void remover(Jogo jogo) {
		jogo = porId(jogo.getId());
		manager.remove(jogo);
	}
}
