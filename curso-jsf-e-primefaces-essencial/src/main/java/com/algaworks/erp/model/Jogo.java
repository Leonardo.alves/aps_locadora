package com.algaworks.erp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "jogo")
public class Jogo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "titulo", nullable = false, length = 255)
	private String titulo;
	
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "plataforma", nullable = false, length = 255)
	private String plataforma;
	
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "genero", nullable = false, length = 255)
	private String genero;
	
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "desenvolvedor", nullable = false, length = 255)
	private String desenvolvedor;
	
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "classificacao_indicativa", nullable = false, length = 255)
	private String classificacaoIndicativa;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getPlataforma() {
		return plataforma;
	}

	public void setPlataforma(String plataforma) {
		this.plataforma = plataforma;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getDesenvolvedor() {
		return desenvolvedor;
	}

	public void setDesenvolvedor(String desenvolvedor) {
		this.desenvolvedor = desenvolvedor;
	}

	public String getClassificacaoIndicativa() {
		return classificacaoIndicativa;
	}

	public void setClassificacaoIndicativa(String classificacaoIndicativa) {
		this.classificacaoIndicativa = classificacaoIndicativa;
	}
	
	
}
