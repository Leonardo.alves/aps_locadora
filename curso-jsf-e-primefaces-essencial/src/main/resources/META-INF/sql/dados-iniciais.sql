insert into ramo_atividade (id, descricao) values (1, 'Distribuição de alimentos');
insert into ramo_atividade (id, descricao) values (2, 'Telecomunicações');
insert into ramo_atividade (id, descricao) values (3, 'Vestuário');
insert into ramo_atividade (id, descricao) values (4, 'Lavanderia');
insert into ramo_atividade (id, descricao) values (5, 'Gráfica');
insert into ramo_atividade (id, descricao) values (6, 'Mecânica');
insert into ramo_atividade (id, descricao) values (7, 'Turismo');
insert into ramo_atividade (id, descricao) values (8, 'Saúde');
insert into ramo_atividade (id, descricao) values (9, 'Educação');
insert into ramo_atividade (id, descricao) values (10, 'Lazer');

insert into jogo (id, titulo, plataforma, genero, desenvolvedor, classificacao_indicativa) values (1, 'teste 1', 'teste 1', 'teste 1', 'teste 1', 'teste 1');
insert into jogo (id, titulo, plataforma, genero, desenvolvedor, classificacao_indicativa) values (2, 'teste 2', 'teste 2', 'teste 2', 'teste 2', 'teste 2');
insert into jogo (id, titulo, plataforma, genero, desenvolvedor, classificacao_indicativa) values (3, 'teste 3', 'teste 3', 'teste 3', 'teste 3', 'teste 3');

insert into cliente (id, nome, endereco, email) values (1, 'Cliente 1', 'Rua 1', 'cliente1@gmail.com');
insert into cliente (id, nome, endereco, email) values (2, 'Cliente 2', 'Rua 2', 'cliente2@gmail.com');
insert into cliente (id, nome, endereco, email) values (3, 'Cliente 3', 'Rua 3', 'cliente3@gmail.com');


insert into empresa (id, cnpj, nome_fantasia, razao_social, tipo, data_fundacao, ramo_atividade_id) values (1, '70.311.193/0001-87', 'Mercado do João', 'João Mercado e Distribuidor de Alimentos Ltda', 'LTDA', '2009-03-02', 1);
insert into empresa (id, cnpj, nome_fantasia, razao_social, tipo, data_fundacao, ramo_atividade_id) values (2, '52.822.994/0001-25', 'Fale Mais', 'Fale Mais Telecom S.A.', 'SA', '1997-12-10', 2);
insert into empresa (id, cnpj, nome_fantasia, razao_social, tipo, data_fundacao, ramo_atividade_id) values (3, '41.952.519/0001-57', 'Maria de Souza da Silva', 'Maria de Souza da Silva', 'MEI', '2014-10-15', 3);
insert into empresa (id, cnpj, nome_fantasia, razao_social, tipo, data_fundacao, ramo_atividade_id) values (4, '16.134.777/0001-89', 'Gomes Inovação', 'José Fernando Gomes EIRELI ME', 'EIRELI', '2009-03-02', 4);